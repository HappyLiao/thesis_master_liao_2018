\select@language {english}
\contentsline {chapter}{\numberline {Chapter 1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Structure of the thesis}{5}{section.1.1}
\contentsline {chapter}{\numberline {Chapter 2}Background}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Traditional Access Control Model}{6}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}ACL}{6}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Capability}{6}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Nowadays Access Control Model}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}MAC}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}DAC}{7}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}RBAC}{8}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}LSM}{9}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}SELinux}{10}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}AppArmor}{11}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}yama}{12}{subsection.2.3.3}
\contentsline {section}{\numberline {2.4}BPF}{12}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Traditional BPF design}{13}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}JIT compiler for BPF}{15}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Interacting with user space:map}{16}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Verifier}{17}{subsection.2.4.4}
\contentsline {subsection}{\numberline {2.4.5}BCC: BPF compiler collection}{18}{subsection.2.4.5}
\contentsline {subsection}{\numberline {2.4.6}BPF work mechanism}{19}{subsection.2.4.6}
\contentsline {chapter}{\numberline {Chapter 3}Design}{20}{chapter.3}
\contentsline {chapter}{\numberline {Chapter 4}Implementation}{24}{chapter.4}
\contentsline {section}{\numberline {4.1}User level implementation}{24}{section.4.1}
\contentsline {section}{\numberline {4.2}Kernel level implementation}{25}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Attribute Setting}{26}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Extract Attribute Information}{27}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Transmit Attribute Information}{29}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Decision Making}{30}{subsection.4.2.4}
\contentsline {chapter}{\numberline {Chapter 5}Evaluation}{31}{chapter.5}
\contentsline {section}{\numberline {5.1}Analysis of Performance}{31}{section.5.1}
\contentsline {section}{\numberline {5.2}Analysis of Usability}{33}{section.5.2}
\contentsline {section}{\numberline {5.3}Analysis of Security}{33}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Attack surface}{33}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Optional use of JIT for eBPF}{34}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Protection against escalation of privileges via supervised process}{34}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Protection of subjects}{35}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Protection against interfering other access controls}{35}{subsection.5.3.5}
\contentsline {section}{\numberline {5.4}Summary}{36}{section.5.4}
\contentsline {chapter}{\numberline {Chapter 6}Related Work}{38}{chapter.6}
\contentsline {section}{\numberline {6.1}seccomp}{38}{section.6.1}
\contentsline {section}{\numberline {6.2}Landlock}{39}{section.6.2}
\contentsline {section}{\numberline {6.3}S.A.R.A}{40}{section.6.3}
\contentsline {section}{\numberline {6.4}Other related work based on LSM framework and dynamic tracing}{40}{section.6.4}
\contentsline {chapter}{\numberline {Chapter 7}Conclusion}{41}{chapter.7}
\contentsline {section}{\numberline {7.1}Future Work}{41}{section.7.1}
\contentsline {chapter}{Acknowledgements}{43}{chapter*.12}
\contentsline {chapter}{Bibliography}{44}{chapter*.13}
